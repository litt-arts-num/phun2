init:
	cp .env.dist .env
	vi .env
	cp application/.env.dist application/.env
	vi application/.env
	docker compose build --no-cache
	docker compose up -d
	docker compose exec apache make init
