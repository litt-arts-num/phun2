<?php

namespace App\Controller;

use App\Entity\Directory;
use App\Entity\MediaServer;
use App\Entity\Project;
use App\Form\MediaServerType;
use App\Service\AppEnums;
use App\Service\CloudMediaFetcher;
use App\Service\MediaManager;
use App\Service\MediaServerManager;
use App\Service\PermissionManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


/**
 * @Route("/media-server", name="media_server_")
 */
class MediaServerController extends AbstractController
{
    private $mediaServerManager;
    private $permissionManager;
    private $translator;

    public function __construct(
        MediaServerManager $mediaServerManager,
        PermissionManager $permissionManager,
        TranslatorInterface $translator
    ) {
        $this->mediaServerManager = $mediaServerManager;
        $this->permissionManager = $permissionManager;
        $this->translator = $translator;
    }

    /**
     * @Route("/{projectId}/edit/{serverId}", name="edit", defaults={"serverId"=null} )
     * @ParamConverter("project", class="App:Project", options={"id" = "projectId"})
     * @ParamConverter("server", class="App:MediaServer", options={"id" = "serverId"})
     */
    public function edit(Project $project, MediaServer $server = null, Request $request)
    {
        if (false === $this->permissionManager->isAuthorizedOnProject($project, AppEnums::ACTION_EDIT_PROJECT)) {
            throw new AccessDeniedException($this->translator->trans('access_denied'));
        }

        if (!$server) {
            $server = $this->mediaServerManager->create($project);
        }

        $form = $this->createForm(MediaServerType::class, $server);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->mediaServerManager->save($server);

            return $this->redirectToRoute('project_edit-media-servers', ['id' => $project->getId()]);
        }

        return $this->render(
            'media-server/edit.html.twig',
            [
                'form' => $form->createView(),
                'project' => $project,
                'server' => $server,
            ]
        );
    }

    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function delete(MediaServer $server)
    {
        $project = $server->getProject();
        if (false === $this->permissionManager->isAuthorizedOnProject($project, AppEnums::ACTION_EDIT_PROJECT)) {
            throw new AccessDeniedException($this->translator->trans('access_denied'));
        }

        $this->mediaServerManager->delete($server);

        return $this->redirectToRoute('project_edit-media-servers', ['id' => $project->getId()]);
    }

    /**
     * @Route("/direct-links/{project}", name="direct_links", options={"expose"=true}, name="fetch_direct_links",
     *     methods="POST")
     */
    public function fetchFilesFromDirectLinks(Request $request, Project $project, CloudMediaFetcher $cloudMediaFetcher)
    {
        $payload = json_decode($request->getContent(), true);

        $links = is_array($payload) && array_key_exists('links', $payload) ? $payload['links'] : null;

        if (!$links) {
            throw new BadRequestHttpException($this->translator->trans('direct_links_empty'));
        }

        return new JsonResponse($cloudMediaFetcher->fetchFilesFromDirectLinks($project, $links));
    }

    /**
     * @Route("/{id}/fetch", name="fetch", options={"expose"=true}, name="fetch_media_files", methods="POST")
     */
    public function fetchFiles(MediaServer $server, Request $request, CloudMediaFetcher $cloudMediaFetcher)
    {
        $payload = json_decode($request->getContent(), true);

        $path = is_array($payload) && array_key_exists('path', $payload) ? $payload['path'] : null;

        if (!$path) {
            throw new BadRequestHttpException($this->translator->trans('media_server_empty_path'));
        }

        return new JsonResponse($cloudMediaFetcher->fetchFilesFromMediaServer($server, $path));
    }

    /**
     * @Route("/{mediaServer}/import/{current}", name="import", requirements={"current"="\d+"},
     *     defaults={"current"=null},
     *     options={"expose"=true}, methods="POST")
     * @Route("/import/project/{project}/{current}", name="import_direct_links", requirements={"current"="\d+"},
     *     defaults={"current"=null},
     *     options={"expose"=true}, methods="POST")
     * @ParamConverter("current", class="App:Directory", options={"id" = "current"})
     */
    public function importCloudMedia(
        Request $request,
        MediaManager $mediaManager,
        TranslatorInterface $translator,
        MediaServer $mediaServer = null,
        Project $project = null,
        Directory $current = null
    ) {
        if (!$mediaManager && !$project) {
            throw new BadRequestHttpException('Project not found');
        }

        $payload = json_decode($request->getContent(), true);

        $medias = is_array($payload) && array_key_exists('files', $payload) ? $payload['files'] : null;

        if (!is_array($medias) || count($medias) <= 0) {
            throw new BadRequestHttpException($translator->trans('invalid_media_items'));
        }

        $project = $project ?: $mediaServer->getProject();

        $medias = $mediaManager->createMediasFromUrls($medias, $project, $current, $mediaServer);

        return new JsonResponse([
            'countImportedItems' => count($medias),
        ]);
    }

    /**
     * @Route("/media", name="proxy_media", options={"expose"=true}, methods="GET")
     */
    public function openFile(Request $request, CloudMediaFetcher $cloudMediaFetcher)
    {
        $path = $request->get('path');

        if (!$path) {
            throw new BadRequestHttpException($this->translator->trans('path_is_missing'));
        }

        return $cloudMediaFetcher->buildFileResponse($path);
    }
}
