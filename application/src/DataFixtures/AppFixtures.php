<?php

namespace App\DataFixtures;

use App\Entity\UserStatus;
use App\Entity\Platform;
use App\Service\AppEnums;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $om)
    {
        // project user statuses
        $transcriber = new UserStatus();
        $transcriber->setName(AppEnums::USER_STATUS_TRANSCRIBER_NAME);
        $transcriber->setDescription(AppEnums::USER_STATUS_TRANSCRIBER_DESC);
        $om->persist($transcriber);
        $validator = new UserStatus();
        $validator->setName(AppEnums::USER_STATUS_VALIDATOR_NAME);
        $validator->setDescription(AppEnums::USER_STATUS_VALIDATOR_DESC);
        $om->persist($validator);
        $manager = new UserStatus();
        $manager->setName(AppEnums::USER_STATUS_MANAGER_NAME);
        $manager->setDescription(AppEnums::USER_STATUS_MANAGER_DESC);
        $om->persist($manager);

        // platform properties
        $platform = new Platform();
        $platform->setName('Please change me');
        $om->persist($platform);
        $om->flush();
    }
}
