<?php

namespace App\Form;

use App\Entity\Platform;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PlatformType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
              'label' => 'platform_name',
              'translation_domain' => 'messages'])
            ->add('displayNameNavBar', ChoiceType::class, [
                'label' => 'Afficher le nom dans la navbar',
                'translation_domain' => 'messages',
                'choices'  => [
                  'yes' => true,
                  'no' => false
                ]])
            ->add('message', TextType::class, [
              'label' => 'platform_message',
              'translation_domain' => 'messages',
              'required' => false])
            ->add('logo', FileType::class, [
              'label' => 'platform_logo',
              'translation_domain' => 'messages',
              'required' => false,
              'data_class' => null])
            ->add('homePicture', FileType::class, [
              'label' => 'Image de la plateforme',
              'translation_domain' => 'messages',
              'required' => false,
              'data_class' => null])
            ->add('color', TextType::class, [
              'label' => 'Couleur principale',
              'translation_domain' => 'messages',
              'required' => false])
            ->add('tesseractUrl', TextType::class, [
              'label' => 'URL Tesseract',
              'required' => false,
              'translation_domain' => 'messages'])
            ->add('questionnaireUrl', TextType::class, [
              'label' => 'URL Questionnaire',
              'required' => false,
              'translation_domain' => 'messages'])
            ->add('mailAdress', TextType::class, [
              'label' => 'Adresse de contact',
              'required' => false,
              'translation_domain' => 'messages'])
            ->add('platform_guide', FileType::class, [
              'label' => 'platform_guide',
              'translation_domain' => 'messages',
              'required' => false,
              'mapped' => false])
            ->add('manager_guide', FileType::class, [
              'label' => 'manager_guide',
              'translation_domain' => 'messages',
              'required' => false,
              'mapped' => false]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Platform::class,
        ));
    }
}
