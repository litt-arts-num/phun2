<?php

namespace App\Service;

use App\Entity\Media;
use App\Entity\MediaServer;
use App\Entity\Project;
use App\Exception\ServiceException;
use App\Model\FileData;
use App\Service\CloudServiceDriver\CloudServiceDriverInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Mime\MimeTypes;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use cardinalby\ContentDisposition\ContentDisposition;

class CloudMediaFetcher
{
    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * @var array
     */
    private $cloudServiceDrivers = [];

    public function __construct(
        HttpClientInterface $client,
        TranslatorInterface $translator,
        FileManager $fileManager,
        iterable $cloudServiceDrivers = []
    ) {
        $this->httpClient = $client;
        $this->translator = $translator;
        $this->fileManager = $fileManager;
        foreach ($cloudServiceDrivers as $driver) {
            $this->registerCloudServiceDriver($driver);
        }
    }

    public function fetchFilesFromMediaServer(MediaServer $server, string $path): array
    {
        $fillPath = $this->getFullPath($server, $path);

        $response = $this->httpClient->request(Request::METHOD_GET, $fillPath);

        if ($response->getStatusCode() !== 200) {
            throw new ServiceException($this->translator->trans('unable_to_fetch_files_from_server'));
        }

        $crawler = new Crawler($response->getContent());

        $linksCrawler = $crawler->filterXPath($server->getMediaXpath());

        $links = [];
        foreach ($linksCrawler as $item) {
            $links[] = $item->attributes['href']->textContent;
        }

        return [
            'path' => $fillPath,
            'medias' => $this->prepareMedias($links, $fillPath, $server),
        ];
    }

    public function fetchFilesFromDirectLinks(Project $project, array $links)
    {
        $result = [
            'medias' => [],
        ];

        $mediasMap = $this->getProjectMediasMap($project);

        foreach ($links as $link) {
            $url = filter_var($link, FILTER_VALIDATE_URL);
            if (!$url) {
                continue;
            }

            $fileResponse = $this->httpClient->request(Request::METHOD_HEAD, $url);

            $foundMedia = array_key_exists($link, $mediasMap) ? $mediasMap[$link] : null;

            $fileData = $this->guessFileData($url, $fileResponse);

            $mediaName = $fileData ? $fileData->getFileName() : null;
            $type = $fileData ? $fileData->getType() : null;

            if (!$foundMedia && $mediaName) {
                $foundMedia = array_search($mediaName, $mediasMap) !== false;
            }

            $thumbnailOptions = $this->generateThumbnail($project, $url, $mediaName, $type);

            $result['medias'][] = [
                'fileName' => $mediaName ?: $url,
                'thumbnailUrl' => $thumbnailOptions['url'],
                'fullThumbnailUrl' => $thumbnailOptions['fullUrl'],
                'url' => $url,
                'type' => $type,
                'exists' => !!$foundMedia,
            ];
        }

        return $result;
    }

    public function buildFileResponse(string $path): Response
    {
        $cloudResponse = $this->httpClient->request(Request::METHOD_GET, $path);

        $headers = $cloudResponse->getHeaders();

        $preparedHeaders = [];
        if (
            array_key_exists('content-type', $headers) &&
            is_array($headers['content-type']) &&
            count($headers['content-type']) > 0) {
            $preparedHeaders['content-type'] = $headers['content-type'][0];
        }

        if (
            array_key_exists('content-disposition', $headers) &&
            is_array($headers['content-disposition']) &&
            count($headers['content-disposition']) > 0) {
            $preparedHeaders['content-disposition'] = str_replace('attachment', 'inline', $headers['content-disposition'][0]);
        }

        $response = new StreamedResponse(
            function () use ($cloudResponse) {
                foreach ($this->httpClient->stream($cloudResponse) as $chunk) {
                    echo $chunk->getContent();
                    flush();
                }
            },
            $cloudResponse->getStatusCode(),
            $preparedHeaders
        );

        return $response;
    }

    private function guessFileData(string $url, ResponseInterface $response): ?FileData
    {
        $headers = $response->getHeaders();

        $fileName = null;
        $type = null;


        if (array_key_exists('content-disposition', $headers) && count($headers['content-disposition']) > 0) {
            $contentDispositionHeader = $headers['content-disposition'][0];

            try {
                $disposition = ContentDisposition::parse($contentDispositionHeader);
                $fileName = $disposition->getFilename();
                $type = Media::getPathType($fileName);
            } catch (\Throwable $throwable) {
            }
        }

        if (!$fileName || !$type) {
            $contentType = array_key_exists('content-type', $headers) ? $headers['content-type'] : null;
            $contentType = is_array($contentType) && count($contentType) > 0 ? $contentType[0] : null;
            if ($contentType) {
                $type = $this->guessFileTypeByContentType($contentType);
            }

            $pathInfo = pathinfo(strtolower($url));
            if (array_key_exists('filename', $pathInfo) && !!$pathInfo['filename'] &&
                array_key_exists('extension', $pathInfo) && !!$pathInfo['extension']) {
                $fileName = $pathInfo['filename'];
            }
        }

        if (!$type) {
            $type = $this->guessFileTypeByUrl($url);
        }

        return !!$fileName && !!$type ? new FileData($fileName, $type) : null;
    }

    private function guessFileTypeByContentType(string $contentType): ?string
    {
        $contentType = strtolower($contentType);
        if ($contentType === 'application/pdf') {
            return Media::TYPE_PDF;
        }

        if (strpos($contentType, 'image') === 0) {
            return Media::TYPE_IMAGE;
        }

        if (strpos($contentType, 'video') === 0) {
            return Media::TYPE_VIDEO;
        }

        if (strpos($contentType, 'audio') === 0) {
            return Media::TYPE_AUDIO;
        }

        return null;
    }

    private function guessFileTypeByUrl(string $url): ?string
    {
        $extension = pathinfo($url, PATHINFO_EXTENSION);

        if (!$extension) {
            return null;
        }

        $mime = new MimeTypes();
        $mimeTypes = $mime->getMimeTypes($extension);

        foreach ($mimeTypes as $mimeType) {
            if ($mimeType === 'application/pdf') {
                return Media::TYPE_PDF;
            }

            if (strpos($mimeType, 'image') === 0) {
                return Media::TYPE_IMAGE;
            }

            if (strpos($mimeType, 'video') === 0) {
                return Media::TYPE_VIDEO;
            }

            if (strpos($mimeType, 'audio') === 0) {
                return Media::TYPE_AUDIO;
            }
        }

        return null;
    }

    private function prepareMedias(array $links, string $requestUrl, MediaServer $server): array
    {
        $mediasMap = $this->getProjectMediasMap($server->getProject());

        return array_map(function (string $link) use ($mediasMap, $server, $requestUrl) {
            $fileName = $this->guessFileName($link, $requestUrl, $server);
            $foundMedia = array_key_exists($link, $mediasMap) ? $mediasMap[$link] : null;

            $type = null;
            $mediaName = null;
            if ($fileName !== $link) {
                $type = Media::getPathType($fileName);
                $mediaName = pathinfo($fileName, PATHINFO_FILENAME);

                if (!$foundMedia && $mediaName) {
                    $foundMedia = array_search($mediaName, $mediasMap) !== false;
                }
            }

            return [
                'fileName' => $mediaName ?: $fileName,
                'url' => $link,
                'type' => $type,
                'exists' => !!$foundMedia,
                'thumbnailUrl' => $this->getThumbnailUrl($link, $requestUrl, $server),
            ];
        }, $links);
    }

    private function generateThumbnail(Project $project, string $url, ?string $mediaName, ?string $type, $width = 512): array
    {
        $result = [
            'url' => null,
            'fullUrl' => null,
        ];

        if ($type !== Media::TYPE_IMAGE || !$mediaName) {
            return $result;
        }

        $fullThumbnailUrl = $this->generateImageThumbnail($project, $url, $mediaName, $width);

        if (!!$fullThumbnailUrl) {
            $result['url'] = basename($fullThumbnailUrl);
            $result['fullUrl'] = $this->fileManager->getPublicProjectPath($project) . $fullThumbnailUrl;
        }

        return $result;
    }

    private function generateImageThumbnail(Project $project, string $url, string $fileName, $width = 512)
    {
        try {
            $imagick = new \Imagick($url);
            $imagick->adaptiveResizeImage($width, 0);
            $path = $this->fileManager->getProjectPath($project);

            $thumbnailPath = DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR . $fileName;
            $resultPath = $path . $thumbnailPath;

            $imagick->writeImage($resultPath);

            return $thumbnailPath;
        } catch (\Throwable $throwable) {
            return null;
        }
    }

    private function getProjectMediasMap(Project $project): array
    {
        $projectMedias = $project->getMedias()->toArray();
        $mediasMap = [];
        /** @var Media $media */
        foreach ($projectMedias as $media) {
            $mediasMap[$media->getUrl()] = $media->getName();
        }

        return $mediasMap;
    }

    private function getFullPath(MediaServer $server, string $path): string
    {
        $preparedPath = str_replace($server->getUrl(), '', $path);

        $result = $server->getUrl();

        if ($server->getUrl()[strlen($server->getUrl()) - 1] !== '/') {
            $result .= '/';
        }

        return $result . ltrim($preparedPath, '/');
    }

    private function guessFileName(string $link, string $requestUrl, MediaServer $server): string
    {
        $driver = $this->getCloudServiceDriver($requestUrl, $server);
        if ($driver) {
            return $driver->getFileName($link, $requestUrl, $server);
        }

        return $link;
    }

    private function getThumbnailUrl(string $link, string $requestUrl, MediaServer $server): ?string
    {
        $driver = $this->getCloudServiceDriver($requestUrl, $server);
        if ($driver) {
            return $driver->getThumbnail($link, $requestUrl, $server);
        }

        return null;
    }

    private function registerCloudServiceDriver(CloudServiceDriverInterface $cloudServiceDriver): void
    {
        $this->cloudServiceDrivers[] = $cloudServiceDriver;
    }

    private function getCloudServiceDriver(string $requestUrl, MediaServer $server): ?CloudServiceDriverInterface
    {
        /** @var CloudServiceDriverInterface $driver */
        foreach ($this->cloudServiceDrivers as $driver) {
            if ($driver->supports($requestUrl, $server)) {
                return $driver;
            }
        }

        return null;
    }
}
