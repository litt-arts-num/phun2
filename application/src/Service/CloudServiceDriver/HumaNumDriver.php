<?php

namespace App\Service\CloudServiceDriver;

use App\Entity\MediaServer;

class HumaNumDriver implements CloudServiceDriverInterface
{
    public function supports(string $requestUrl, MediaServer $server): bool
    {
        $host = parse_url($server->getUrl(), PHP_URL_HOST);

        return $host === 'sharedocs.huma-num.fr';
    }

    public function getThumbnail(string $fileUrl, string $requestUrl, MediaServer $server): ?string
    {
        $queryString = parse_url($requestUrl, PHP_URL_QUERY);

        $path = str_replace($queryString, '', $requestUrl);

        parse_str($queryString, $query);

        $fileName = $this->getFileName($fileUrl, $requestUrl, $server);

        if (array_key_exists('path', $query)) {
            $query['path'] = $query['path'] . '/' . $fileName;

            $query['mode'] = 'grid';
            $query['thumbnail'] = '1';

            return $path . http_build_query($query);
        }

        return null;
    }

    public function getFileName(string $fileUrl, string $requestUrl, MediaServer $server): string
    {
        $query = parse_url($fileUrl, PHP_URL_QUERY);

        parse_str($query, $params);

        if (array_key_exists('path', $params)) {
            return basename($params['path']);
        }

        return $fileUrl;
    }
}
