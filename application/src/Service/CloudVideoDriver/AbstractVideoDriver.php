<?php

namespace App\Service\CloudVideoDriver;

use Symfony\Component\String\Slugger\AsciiSlugger;

abstract class AbstractVideoDriver implements CloudVideoDriverInterface
{
    protected function prepareVideoFileName(string $videoName): string
    {
        $slugger = new AsciiSlugger();
        $slugger->setLocale('fr');

        return $slugger->slug($videoName);
    }
}
