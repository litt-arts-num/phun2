<?php

namespace App\Service\CloudVideoDriver;

class CloudVideoService
{
    private $cloudServiceDrivers;

    public function __construct(iterable $cloudServiceDrivers)
    {
        $this->cloudServiceDrivers = $cloudServiceDrivers;
    }

    public function getVideoData(string $type, string $videoId): VideoInfo
    {
        return $this->getDriver($type)->getVideoData($videoId);
    }

    private function getDriver(string $type): CloudVideoDriverInterface
    {
        /** @var CloudVideoDriverInterface $driver */
        foreach ($this->cloudServiceDrivers as $driver) {
            if ($driver->getType() === $type) {
                return $driver;
            }
        }

        throw new \RuntimeException(sprintf('Unable to find video driver for type "%s"', $type));
    }
}
