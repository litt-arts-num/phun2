<?php

namespace App\Service\CloudVideoDriver;

use App\Entity\Media;
use App\Exception\ServiceException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class VimeoDriver extends AbstractVideoDriver
{
    private const URL = 'https://vimeo.com/api/v2/video';

    private $httpClient;

    /**
     * @param $httpClient
     */
    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getType(): string
    {
        return Media::VIDEO_TYPE_VIMEO;
    }

    public function getVideoData(string $videoId): VideoInfo
    {
        $response = $this->httpClient->request(
            Request::METHOD_GET,
            sprintf('%s/%s.%s', self::URL, $videoId, 'json')
        );

        $responseData = $response->toArray();
        if (count($responseData) <= 0) {
            throw new ServiceException('Unable to get vimeo video info');
        }

        $videoData = $responseData[0];

        return new VideoInfo(
            $videoData['url'],
            $this->prepareVideoFileName($videoData['title']),
            $videoData['thumbnail_medium']
        );
    }
}
