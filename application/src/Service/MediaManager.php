<?php

namespace App\Service;

use App\Entity\Directory;
use App\Entity\IiifServer;
use App\Entity\Media;
use App\Entity\MediaServer;
use App\Entity\Project;
use App\Entity\Transcription;
use App\Entity\TranscriptionLog;
use App\Exception\ServiceException;
use App\Repository\MediaRepository;
use App\Service\AppEnums;
use App\Service\CloudVideoDriver\CloudVideoService;
use App\Service\FileManager;
use App\Service\MetadataManager;
use App\Service\TranscriptionManager;
use Doctrine\ORM\EntityManagerInterface;
use Jfcherng\Diff\Differ;
use Jfcherng\Diff\DiffHelper;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\Security;

class MediaManager
{
    protected $em;
    protected $security;
    protected $transcriptionManager;
    protected $metadataManager;
    protected $fileManager;

    protected $cloudVideoService;

    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        TranscriptionManager $transcriptionManager,
        MetadataManager $metadataManager,
        FileManager $fileManager,
        CloudVideoService $cloudVideoService
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->transcriptionManager = $transcriptionManager;
        $this->metadataManager = $metadataManager;
        $this->fileManager = $fileManager;
        $this->cloudVideoService = $cloudVideoService;
    }

    public function createMediaFromIIIF(string $identifier, string $fileClientName, Project $project, Directory $parent = null, IiifServer $server)
    {
        $name = $this->fileManager->getFileNameWitoutExt($fileClientName);
        $media = new Media();
        $media->setUrl($identifier);
        $media->setName($name);
        $media->setParent($parent);
        $media->setProject($project);
        $media->setIiifServer($server);
        $project->addMedia($media);
        $media = $this->initMediaTranscription($media);

        //En attente de la gestion des métadonnées interrogeables sur un serveur IiifServer
        $this->metadataManager->applyToMedia($media);

        $this->em->persist($project);
        $this->em->persist($media);
        $this->em->flush();

        return $media;
    }

    public function createCloudVideoMedia(Project $project, string $videoId, string $serviceType, Directory $parent = null)
    {
        $videoInfo = $this->cloudVideoService->getVideoData($serviceType, $videoId);

        if (!$videoInfo->isValid()) {
            throw new ServiceException('Unable to fetch video info');
        }

        $media = new Media();
        $media->setUrl($videoId);
        $media->setName($videoInfo->getTitle());
        $media->setThumbnailUrl($videoInfo->getThumbnailUrl());
        $media->setType(Media::TYPE_VIDEO);
        $media->setVideoType($serviceType);
        $media->setParent($parent);
        $media->setProject($project);
        $project->addMedia($media);
        $media = $this->initMediaTranscription($media);
        $this->metadataManager->applyToMedia($media);

        $this->em->persist($project);
        $this->em->persist($media);
        $this->em->flush();

        return $media;
    }

    public function createMediaFromFile(File $file, string $fileClientName, Project $project, Directory $parent = null)
    {
        $name = $this->fileManager->getFileNameWitoutExt($fileClientName);

        $extension = pathinfo($fileClientName, PATHINFO_EXTENSION);
        $media = new Media();
        $media->setUrl(md5(uniqid()) . '.' . $extension);
        $media->setType(Media::getPathType($fileClientName));
        if ($media->isVideo()) {
            $media->setVideoType(Media::VIDEO_TYPE_LOCAL);
        }
        $media->setName($name);
        $media->setParent($parent);
        $media->setProject($project);
        $project->addMedia($media);
        $media = $this->initMediaTranscription($media);
        $this->metadataManager->applyToMedia($media);

        $this->em->persist($project);
        $this->em->persist($media);
        $this->em->flush();

        return $media;
    }

    public function createMediaFromUrl(string $url, string $fileName, Project $project)
    {
        $media = new Media();
        $media->setUrl($url);
        $media->setName($fileName);
        $media->setProject($project);
        $project->addMedia($media);
        $media = $this->initMediaTranscription($media);
        $this->metadataManager->applyToMedia($media);

        $this->em->persist($project);
        $this->em->persist($media);
        $this->em->flush();

        return $media;
    }

    public function findMediasByIds(array $ids): iterable
    {
        return $this->em->getRepository(Media::class)->findBy(['id' => $ids]);
    }

    public function findProjectMediaByUrlAndType(Project $project, string $url, string $type): ?Media
    {
        /** @var MediaRepository $mediaRepository */
        $mediaRepository = $this->em->getRepository(Media::class);

        return $mediaRepository->getByProjectAndUrl($project, $url, $type);
    }

    public function createMediasFromUrls(array $linkItems, Project $project, Directory $parent = null, MediaServer $mediaServer = null)
    {
        $result = [];

        /** @var MediaRepository $mediaRepository */
        $mediaRepository = $this->em->getRepository(Media::class);

        foreach ($linkItems as $item) {
            if ($mediaRepository->hasMediaUrlInProject($project, $item['url']) ||
                $mediaRepository->hasFileNameInProject($project, $item['fileName'])) {
                continue;
            }

            $media = new Media();
            $media->setUrl($item['url']);
            $media->setName($item['fileName']);

            if (array_key_exists('thumbnailUrl', $item) && $item['thumbnailUrl']) {
                $media->setThumbnailUrl($item['thumbnailUrl']);
            }

            if (array_key_exists('type', $item) && $item['type']) {
                $media->setType($item['type']);
            }

            $media->setProject($project);
            $project->addMedia($media);
            $media->setParent($parent);
            $media = $this->initMediaTranscription($media);
            $this->metadataManager->applyToMedia($media);
            $this->em->persist($media);
            $result[] = $media;

            if ($mediaServer) {
                $mediaServer->addMedia($media);
            }
        }

        if ($mediaServer) {
            $this->em->persist($mediaServer);
        }
        $this->em->flush();

        return $result;
    }

    public function createMediaFromNothing(string $fileClientName, Project $project, string $content, Directory $parent = null)
    {
        $name = $this->fileManager->getFileNameWitoutExt($fileClientName);
        $media = new Media();
        $media->setUrl(null);
        $media->setName($name);
        $media->setParent($parent);
        $media->setProject($project);
        $project->addMedia($media);
        $media = $this->initMediaTranscription($media, $content);
        $this->metadataManager->applyToMedia($media);

        $this->em->persist($project);
        $this->em->persist($media);
        $this->em->flush();

        return $media;
    }

    public function initMediaTranscription(Media $media, $content = '')
    {
        $transcription = new Transcription();
        $transcription->setContent($content);
        $this->transcriptionManager->addLog($transcription, AppEnums::TRANSCRIPTION_LOG_CREATED);
        $media->setTranscription($transcription);

        return $media;
    }

    public function setMediaTranscription(Media $media, string $content)
    {
        $transcription = $media->getTranscription();
        $oldContent = $transcription->getContent();

        if ($oldContent !== $content) {
            $differOptions = [
                'context' => 0,
                'ignoreCase' => false,
                'ignoreWhitespace' => false,
            ];

            $breaks = ["<br />", "<br>", "<br/>"];
            $jsonDiff = DiffHelper::calculate(str_ireplace($breaks, "\r\n", $oldContent), str_ireplace($breaks, "\r\n", $content), 'Json', $differOptions);

            $transcription->setContent($content);
            $this->transcriptionManager->addLog($transcription, AppEnums::TRANSCRIPTION_LOG_UPDATED, $jsonDiff);
            $this->em->persist($transcription);
            $this->em->flush();
        }

        return;
    }

    public function save(Media $media)
    {
        $this->em->persist($media);
        $this->em->flush();

        return $media;
    }

    public function generateURL(Media $media)
    {
        if ($media->getUrl() && $media->getIiifServer() == null) {
            if ($media->isCloudUrl()) {
                return $media->getUrl();
            }

            $url = '/project_files/' . $media->getProject()->getId() . "/" . $media->getUrl();
        } elseif ($media->getIiifServer()) {
            $url = $media->getIiifServer()->getUrl() . $media->getIiifServer()->getPrefixLarge() . $media->getUrl() . $media->getIiifServer()->getSuffixLarge();
        } else {
            $url = '/img/media_placeholder.jpg';
        }

        return $url;
    }

    public function getIIIFInfos($absolutePath)
    {
        $xml = file_get_contents($absolutePath);
        preg_match('/<([^<:]+:)?identifier>([^<]+)<\/([^<:]+:)?identifier>/', $xml, $matches);
        if (array_key_exists(2, $matches)) {
            return $matches[2];
        }

        return null;
    }
}
