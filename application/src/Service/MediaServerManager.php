<?php

namespace App\Service;

use App\Entity\IiifServer;
use App\Entity\MediaServer;
use App\Entity\Project;
use App\Service\FlashManager;
use Doctrine\ORM\EntityManagerInterface;

class MediaServerManager
{
    protected $em;
    protected $fm;

    public function __construct(
        EntityManagerInterface $em,
        FlashManager $fm
    ) {
        $this->em = $em;
        $this->fm = $fm;
    }

    public function create(Project $project)
    {
        $server = new MediaServer();
        $server->setProject($project);

        return $server;
    }

    public function save(MediaServer $server)
    {
        $this->em->persist($server);
        $this->em->flush();

        $this->fm->add('notice', 'Serveur sauvegardé');

        return;
    }

    public function delete(MediaServer $server)
    {
        $mediaCount = count($server->getMedias());
        if ($mediaCount == 0) {
            $this->em->remove($server);
            $this->em->flush();
            $this->fm->add('notice', 'Serveur supprimé');
        } else {
            $this->fm->add('notice', 'Serveur non supprimé. Des médias ('.$mediaCount.') sont encore liés à ce serveur.');
        }

        return;
    }
}
