<?php

namespace App\Tests\Service;

use App\Entity\Media;
use App\Entity\MediaServer;
use App\Entity\Project;
use App\Service\CloudMediaFetcher;
use App\Service\CloudServiceDriver\HumaNumDriver;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\Translation\Translator;

class CloudMediaFetcherTest extends KernelTestCase
{
    public function testFetchFromHumaNum()
    {
        $media = new Media();
        $media->setUrl('http://some.example.com');

        $project = new Project();
        $project->addMedia($media);

        $mediaServer = new MediaServer();
        $mediaServer->setProject($project);

        $mediaServer->setUrl('https://sharedocs.huma-num.fr/');
        $mediaServer->setMediaXpath('//a[contains(@class, "filenameRow")]');

        $fetcher = $this->getPdfFetcher();

        $result = $fetcher->fetchFilesFromMediaServer($mediaServer, 'https://sharedocs.huma-num.fr/wl/?id=b5YJ1qQ6vIHC3W8wz5BtGO1IGfoNZUP6&path=PDF&mode=grid');

        $this->assertIsArray($result['medias']);
        $this->assertGreaterThan(0, count($result['medias']));

        $firstLink = $result['medias'][0];

        $this->assertEquals(
            'https://sharedocs.huma-num.fr/wl/?id=b5YJ1qQ6vIHC3W8wz5BtGO1IGfoNZUP6&path=PDF%2FPV1959-03-13.pdf&mode=grid&download=1',
            $firstLink['url']
        );

        $this->assertEquals(
            'PV1959-03-13.pdf',
            $firstLink['fileName']
        );

        $this->assertEquals(
            'https://sharedocs.huma-num.fr/wl/?id=b5YJ1qQ6vIHC3W8wz5BtGO1IGfoNZUP6&path=PDF%2F' .
            $firstLink['fileName'] .
            '&mode=grid&thumbnail=1',
            $firstLink['thumbnailUrl']
        );
    }

    public function testFetchMediaItems()
    {
        $project = new Project();

        $mediaServer = new MediaServer();
        $mediaServer->setProject($project);

        $mediaServer->setUrl('https://sharedocs.huma-num.fr/');
        $mediaServer->setMediaXpath('//a[contains(@class, "filenameRow")]');

        $fetcher = $this->getAudioMediaFetcher();

        $result = $fetcher->fetchFilesFromMediaServer($mediaServer, 'https://sharedocs.huma-num.fr/wl/?id=BPjpSToKLrD4BQT7TJ1eO3ZgRc8YjjAx');

        $this->assertIsArray($result['medias']);
        $this->assertGreaterThan(0, count($result['medias']));
    }

    private function getPdfFetcher(): CloudMediaFetcher
    {
        $mockResponse = new MockResponse(file_get_contents(__DIR__ . '/../Resources/huma-num.pdf.html'));
        $mockHttpClient = new MockHttpClient($mockResponse);

        return new CloudMediaFetcher(
            $mockHttpClient,
            new Translator('fr'),
            [new HumaNumDriver()]
        );
    }

    private function getAudioMediaFetcher(): CloudMediaFetcher
    {
        $mockResponse = new MockResponse(file_get_contents(__DIR__ . '/../Resources/huma-num.wav.html'));
        $mockHttpClient = new MockHttpClient($mockResponse);

        return new CloudMediaFetcher(
            $mockHttpClient,
            new Translator('fr'),
            [new HumaNumDriver()]
        );
    }
}
